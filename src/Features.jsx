import React from 'react';

import { useInViewport } from './tools.jsx';

import {FEATURES} from './content';

function Feature({title,key, description, img, flipped=false}) {
    const [visible, id] = useInViewport();
    return (
        <div id={id} key={key} style={{
           // opacity: (visible)?1:0,
            //transition:'opacity .4s',
            width:'100%',
            display:'flex',
            flexDirection:(flipped)?'row-reverse':'row',
            //flexDirection:'column',
            alignItems:'center',
            justifyContent:'center',
            flexWrap:'wrap',
            padding:'2rem 0rem',
            height:'min-content'
        }}>
          <img alt="title" src={img} style={{
              width:'100%',
              margin:'0 2rem',
              //transform: `rotateY(${(visible)?0:90}deg)`,
              transform: `scale(${(visible)?100:0}%)`,
              transition:'transform .4s'
          }}/>
          <div style={{
              opacity: (visible)?1:0,
              transition:'opacity .4s',
              fontFamily:'Poiret One',
              margin:'0 1rem',
          }}>
            <p style={{
                fontFamily:'Poiret One',
                fontSize:'1.5rem'
            }}>
              {title}
            </p>
            {description}
          </div>
        </div>
    );
}


function FeaturesView({lang}) {
    return (
        <div style={{
            display:'flex',
            flexDirection:'column'
        }} id='features'>
          {FEATURES.map(({img, title, description}, index) => {
              return (
                  <Feature img={img} key={'feature-'+index} title={title[lang]} description={description[lang]} flipped={index%2===0}/>
              );
          })}
        </div>
    );
}

export {
    FeaturesView
}
