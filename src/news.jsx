import * as Feather from 'react-feather';
import React from 'react';

const NEWS = [
   /* {
        icon: <Feather.Code />,
        title:{
            de:'Version 1 SmallWorld veröffentlich',
            en:'Released Version1 SmallWorld'
        },
        content: {
            de:'Clicke hier für mehr Info',
            en: 'Click for more information'
        },
        link: ''
        },*/
        {
            icon: <Feather.Compass />,
            title:{
                de:'Version 1 SmallWorld ReleaseDate',
                en:'Released Version1 ReleaseDate'
            },
            content: {
                de:'Freitag, 16.10.2020 21:00 GMT+2',
                en: 'Friday, 16.10.2020 21:00 GMT+2'
            },
            link: 'https://lbry.tv/@turtleos:7/releasedate-turtleos-smallworld:3'
        },
    {
        icon: <Feather.Bell />,
        title:{
            en:'TurtleOS SmallWorld is out now',
            de:'TurtleOS SmallWorld ist jetzt erhältlich'
        },
        content: {
            de:'Die neusten Infos auf lbry.tv',
            en: 'Click to read about this release'
        },
        link: 'https://lbry.tv/@turtleos:7/release-v1-smallworld:6'
    },
];

export {
    NEWS
}
