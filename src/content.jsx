import * as Feather from 'react-feather';
import React from 'react';

import featuresempty from './assets/platform-renders/mixed/empty.png';
import featuresappmenu from './assets/platform-renders/mixed/appmenu.png';
import featuresterminal from './assets/platform-renders/mixed/terminal.png';

import mobileempty from './assets/platform-renders/mobile/mobile-empty-wide.png';
import desktopempty from './assets/platform-renders/desktop/desktop-empty.png';


const FEATURES = [
    {
        img: featuresempty,
        title: {
            de:'Fühl dich immer Willkommen',
            en:'Always feel home',
        },
        description: {
            de:'TurtleOS ist dein und wird dein bleiben. ',
            en:'TurtleOS is yours and if you want to change something - you can'
        }
    }, {
        img: featuresappmenu,
        title: {
            de:'Geräteoptimierte Oberfläche',
            en:'Platform optimised UI',
        },
        description: {
            de:'Die TurtleOS Oberfläche ist für jede Platform optimiert und erlaubt es dir dein Lieblingsbetriebssystem immer zu nutzen',
            en:'We\'ve optimised the TurtleOS UI to work on every Platform just the best way'
        }
    }, {
        img: featuresterminal,
        title: {
            de:'Zwei Geräte - eine Software',
            en:'Different devices - Same Software',
        },
        description: {
            de:'Genervt vom Softwarewechsel? In TurtleOS läuft deine Software sowohl auf dem Handy als auch auf dem Rechner',
            en:'Tired of having to use diffrent apps on diffrent devices? Wait no longer! TurtleOS allows you to run the same Software on your Smartphone and your Computer'
        }
    },
];

const DOWNLOADS = [
    {
        img: mobileempty,
        title: {
            de:'Mobile',
            en:'Phone'
        },
        options: [
            {
                name: {
                    de: 'Android',
                    en: 'Android'
                },
                url: ''
            },
            {
                name: {
                    de:'Andere',
                    en: 'Other'
                },
                url: 'https://app.turtleos.ccw.icu'
            }
        ]
    }, {
        img: desktopempty,
        title: {
            de:'Desktop',
            en:'Desktop'
        },
        options: [
            {
                name: {
                    de: 'Linux',
                    en: 'Linux'
                },
                url: ''
            },
            {
                name: {
                    de:'Andere',
                    en: 'Other'
                },
                url: 'https://app.turtleos.ccw.icu'
            }
        ]
    },

];

const MEDIABUTTONS = [
    {
        icon: <Feather.Tv size={96}/>,
        color: '#64DD17',
        onClick: ()=>{
            window.open('https://lbry.tv/@turtleos');
        }
    }
];

export {
    FEATURES,
    DOWNLOADS,
    MEDIABUTTONS
}
