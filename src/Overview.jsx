import React from 'react';

function MediaButton({icon, color, onClick=()=>{}}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div style={{
            background: (hovered)?'#E0E0E0':'#F5F5F5',
            boxShadow:'0px 0px 2px rgba(0,0,0,0.4)',
            transition:'background .2s, filter .2s',
            color:(hovered)?color:'#8f8f8f',
            padding:'1rem',
            height:'1rem',
            width:'1rem',
            borderRadius:'50%',
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            margin:'0.4rem',
            strokeWidth:'0.15rem'
        }} onMouseEnter={()=>setHovered(true)}
             onMouseLeave={()=>setHovered(false)}
             onClick={onClick}
        >
          {icon}
        </div>
    );
}
function HomeButton({text, onClick=()=>{}, animated=false, icon}) {
    const cont = text.split("");

    const [hovered, setHovered] = React.useState(false);
    const [lim, setLim] = React.useState(-1);

    React.useEffect(()=>{
        if(animated) {
            let interval = setInterval(()=>{
                let c = lim+1;
                if(c>cont.length) c=-1;
                setLim(c);
            }, 300);
            return ()=>{
                clearInterval(interval);
            };
        } else {
            setLim(cont.length);
        }
        return ()=>{};
    },[lim]);

    return (
        <div style={{
            background:(!hovered)?'rgba(0,0,0, 0.6)':'rgba(0,0,0, 0.7)',
            transition:'background .2s',
            padding:'0.6rem 1rem',
            margin:'0.4rem',
            borderRadius:'15px',
            lineheight:'0.6rem',
            fontSize:'1.2rem',
            display:'flex',
            flexDirection:'row',
            whiteSpace:'break-spaces',
            alignItems:'center',
            justifyContent:'space-between'
        }}
             onMouseEnter={()=>{setHovered(true);}}
             onMouseLeave={()=>setHovered(false)}
             onClick={onClick}
        >
          {[...cont].map((t, index)=>{
              return <p style={{
                  opacity:(lim>=index)?1:0,
                  transition:'opacity .1s',
                  margin:0
              }}>
                       {t}
                     </p>;
          })}
          <div style={{marginLeft:'0.4rem',
                       color:(hovered)?'#F57C00':'white',
                       transition:'color .2s',
                       strokeWidth:'0.08rem'
                      }}>
        {icon}
        </div>
        </div>
    );
}


export {
    MediaButton,
    HomeButton
}
