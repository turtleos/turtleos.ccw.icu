/*
  Asset imports
*/
//Desktop
import desktopScreenshotEmpty from './assets/platform-renders/v1-desktop-empty.png';
import desktopScreenshotMenu from './assets/platform-renders/v1-desktop-menu.png';
import desktopScreenshotWindows from './assets/platform-renders/v1-desktop-windows.png';


const platforms = {
    desktop: [
        {
            title: {
                de: 'Minimalistische UI',
                en: 'Minimalistic UI'
            },
            screenshot: desktopScreenshotEmpty,
            desc: {
                en: `The TurtleOS Desktop Version ships with a minimalistic UI, which is optimised for devices controlled with mouse and keyboard.`,
                de: `TurtleOS Desktop besitzt eine minimalistische Oberfläche, die für die nutzung mit Maus und Tastatur Optimiert ist`
            }
        },
        {
            title: {
                de: 'Ein übersichtliches Menu',
                en: 'A Simplistic Menu'
            },
            screenshot: desktopScreenshotMenu,
            desc: {
                en: `The simplistic app drawer allows for easy use and won't distract you from your work`,
                de: `Das simple gestaltete App Menu ist einfach in der Nutzung und wird dich nicht beim arbeiten ablenken`
            }
        },
        {
            title: {
                de: 'Fliegende Fenster',
                en: 'Floating Windows'
            },
            screenshot: desktopScreenshotWindows,
            desc: {
                en: `Our innovative Window design makes your desktop look even better`,
                de: `Unser innovatives Fensterdesign lässt deinen Desktop noch besser aussehen`
            }
        }
    ]
};

export {
    platforms
}
