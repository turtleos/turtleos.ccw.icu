import React from 'react';
import * as uuid from 'uuid';


function useInViewport(id1) {
    const [vis, setVis] = React.useState(false);
    if(!id1 || id1==="") id1=uuid.v4();
    const [id] = React.useState(id1);

    const isInViewport = (el) => {
        if(el) {
            const rect = el.getBoundingClientRect();
            let screen_height =  (window.innerHeight || document.documentElement.clientHeight);
            let screen_width =  (window.innerWidth || document.documentElement.clientWidth);
            let dH = rect.height;
            let dW = rect.width;
            return (
                rect.top >= 0-dH &&
                    rect.left >= 0-dW &&
                    rect.bottom <= screen_height +dH &&
                    rect.right <= screen_width + dW);

        }
        return false;
    };

    React.useEffect(()=>{

        let i = setInterval(()=>{
            let el = document.getElementById(id);
            if(el) {
                setVis(isInViewport(el));
            }
        },200);
        return ()=>{clearInterval(i);};
    }, [vis]);
    return [vis, id];
}

function AnimatedView({id, view}) {
    const [vis] = useInViewport(id);
    return (
        <div id={id} style={{
            opacity:(vis)?1:0,
            transition:'opacity 2s'
        }}>
          {view}
        </div>
    );
}
export {
    useInViewport,
    AnimatedView,
}
