const translations = {
    title:'TurtleOS',
    status:{
        de:'Das OS für jede Platform',
        en:'One OS for every Platform'
    },
    run: {
        de:'Ausprobieren',
        en:'Try now'
    }
};
export {
    translations
}
