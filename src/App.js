import React from 'react';

import logo from './assets/turtle-assets/turtle.png';
import cover from './assets/cover.jpg';
import bg from './assets/bg.jpg';


import * as Feather from 'react-feather';

import { Scrollbars } from 'react-custom-scrollbars';

import GoogleFontLoader from 'react-google-font-loader';

import './App.css';
import { useInViewport } from './tools';
import { HomeButton, MediaButton } from './Overview';
import { FeaturesView } from './Features';
import { DownloadView } from './Download';
import { MEDIABUTTONS } from './content';

function MenuItem({text, href, onClick=()=>{}}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div style={{
            flex:1,
            width:'100%',
            background:(hovered)?'#181818':'#212121',
            transition:'background .2s',
            display:'flex',
            alignItems:'center',
            justifyContent:'center'
        }}
             onMouseEnter={()=>setHovered(true)}
             onMouseLeave={()=>setHovered(false)}
             onClick={onClick}
        >
          <p href={href} style={{
              color:'white'
          }}>{text}</p>
        </div>
    );
}





function App() {
    const [showMenu, setShowMenu] = React.useState(false);
    const [lang, setLang] = React.useState('en');
    const [logo_vis, logo_id] = useInViewport();
  return (
      <div className="App" style={{
          height:'100%',
          width:'100%',
          position:'fixed',
          top:0,
          left:(showMenu)?'-10rem':0,
          transition:'left .2s',
          userSelect:'none',
          background:'#e4e4e4'
      }} onContextMenu={(e)=>{e.preventDefault();}}>
        <GoogleFontLoader fonts={[
            {
                font:'Poiret One',
                weights: [400]
            },
            {
                font:'Open Sans',
                weights: [400]
            }
        ]}/>
        <Scrollbars>
          <div className="menu-btn" onClick={()=>{
              setShowMenu(!showMenu);
          }} style={{
              color:'white',
              position:'fixed',
              top:0,
              right:(showMenu)?'10rem':0,
              margin:'1rem',
              zIndex:10,
              transition:'right .2s'
          }}>
            {(showMenu)?<Feather.X/>:<Feather.Menu/>}
          </div>
          <div id="home">
            <div className="cover" style={{
                background: `url(${cover}) no-repeat center center fixed`,
                backgroundSize:'cover',
                color:'white',
                fontFamily:'Poiret One',
                display:'flex',
                flexDirection:'column',
                alignItems:'center',
                justifyContent:'center',
                fontSize:'3rem',
                padding:'1rem 0',
                position:'relative'
            }}>
              <div style={{
                  margin:'2rem',
                  display:'flex',
                  flexDirection:'column',
                  alignItems:'center',
                  justifyContent:'center',
              }}>
                <img id={logo_id} alt="LOGO" src={logo} style={{
                    width:'35vmin',
                    filter: `saturate(${(logo_vis)?100:0}%)`,
                    transition:'filter .8s'
              }}/>
              TurtleOS
              </div>
              <div className="buttonbar" style={{
                  display:'flex',
                  flexDirection:'row',
                  justifyContent:'space-evenly',
                  margin:'2rem 1rem',
                  flexWrap:'wrap'
              }}>
                <HomeButton text="Try now!" icon={<Feather.Play strokeWidth={1.5} size={32}/>} animated={true} onClick={()=>{
                    let el = document.getElementById('download');
                    if(el) {
                        el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
                    }
                }}/>
                <HomeButton text="Learn more" icon={<Feather.ArrowDown strokeWidth={1.5} size={32}/>} animated={false} onClick={()=>{
                    let el = document.getElementById('features');
                    if(el) {
                        el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
                    }
                }}/>
              </div>
            <div className="mediabuttons" style={{
                position:'absolute',
                right:'0.4rem',
                bottom:'-1.8rem',
                display:'flex',
                flexDirection:'row',
                flexWrap:'wrap'
            }}>
              {MEDIABUTTONS.map(({icon, color, onClick})=>{
                  return <MediaButton icon={icon} color={color} onClick={onClick}/>;
              })}
      </div>
      </div>
          </div>
          {/*Embedds feature View*/}
            <FeaturesView lang={lang}/>
          {/*Embedds download View*/}
          <DownloadView lang={lang}/>

          <div className="bottombar" id="bottombar" style={{
              padding:'2rem 0',
              background:'#2c3e50'
          }}>
            
          </div>
      </Scrollbars>
        <div className="sidebar" style={{
            position:'absolute',
            top:0,
            left:'100%',
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            justifyContent:'space-around',
            height:'100%',
            width:'10rem'
        }} onClick={()=>setShowMenu(false)}>
          <MenuItem text={"Home"} onClick={()=>{
              let el = document.getElementById('home');
              if(el) {
                  el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
              }
          }}/>
          <MenuItem text={"Features"} onClick={()=>{
              let el = document.getElementById('features');
              if(el) {
                  el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
              }
          }}/>
          <MenuItem text={"Downloads"} onClick={()=>{
              let el = document.getElementById('download');
              if(el) {
                  el.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
              }
          }}/>
        </div>
    </div>
  );
}

export default App;
