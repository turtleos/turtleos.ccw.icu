import React from 'react';

import { useInViewport } from './tools.jsx';

import {DOWNLOADS} from './content';

function DownloadItem({img, title, lang, options, seperator}) {
    const [vis, id] = useInViewport();
    return (
        <div style={{
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            justifyContent:'center',
            marginBottom:(seperator)?'3rem':'1rem'
        }} id={id} key={title}>
          <img alt={title} src={img} style={{
              transform: `scale(${(vis)?100:0}%)`,
              transition:'transform .4s',
              width:'80%'
          }}/>
          <p style={{
              fontSize:'1.4rem'
          }}>
            {title}
          </p>
          <div style={{
              display:'flex',
              flexDirection:'row',
              flexWrap:'wrap',
              whiteSpace:'break-spaces',
              fontFamily:'monospace'
          }}>
          {options.map(({name, url}, index, arr)=>{
              return (<p style={{
                  margin:0,
                  color:'#00838F'
              }} onClick={()=>{
                  window.open(url);
              }}>
                        {name[lang]}{(index<arr.length-1)?<span style={{
                            color:'#212121',
                            margin:0
                        }}> | </span>:''}
                      </p>);
          })}
          </div>
        </div>
    );
}

function DownloadView({lang}) {
    const [vis, id] = useInViewport();
    return (
        <div style={{
            display:'flex',
            flexDirection:'column',
            alignItems:'center',
            justifyContent:'center',
            fontFamily:'Poiret One',
                   }} id="download">
          <p style={{
              fontSize:'2rem',
              marginTop:'10rem',
              opacity:(vis)?1:0,
              transition: 'opacity .2s'

          }} id={id}>
            DOWNLOADS
          </p>
          {DOWNLOADS.map(({options, title, img}, index, arr)=>{
              return <DownloadItem img={img} options={options} lang={lang} title={title[lang]} seperator={index<arr.length-1}/>;
          })}
        </div>
    );
}
export {
    DownloadView
}
